import conditions.pre.*;

public class Arithmetic {
  public static void main(String[] args) {
    new Arithmetic().divide(5, 2);  
  }
  
  @Requires({"divisible"})
  public int divide(int num, @Not(0) int denom) {
    return num / denom;
  }

  protected boolean divisible(int num, int denom) {
    return num % denom == 0;
  }
}
