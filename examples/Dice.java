import conditions.pre.MinValue;
import conditions.pre.MaxValue;

public class Dice {
  public static int sumDice(
      @MinValue(1) @MaxValue(6) int x,
      @MinValue(1) @MaxValue(6) int y)
  {
    return x + y;
  }

  public static void main(String[] args) {
    System.out.println(sumDice(2, 5));
    System.out.println(sumDice(1, 7));
  }
}
