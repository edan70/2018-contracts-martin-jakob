import conditions.pre.NotNull;
import conditions.pre.Requires;
import conditions.pre.PreconditionViolationException;
import java.util.Deque;
import java.util.LinkedList;

public class Stack<E> {

  private Deque<E> data;

  public Stack() {
    data = new LinkedList<>();
  }

  public Stack(@NotNull Deque<E> data) {
    this.data = data;
  }

  public void push(@NotNull E elem) {
    data.push(elem); 
  }

  @Requires({"nonEmpty"})
  public E pop() {
    return data.pop();
  }

  @Requires({"nonEmpty"})
  public E peek() {
    return data.peek();
  }

  private boolean nonEmpty() {
    return data.size() > 0;
  }

  public static void main(String[] args) {
    Stack<String> stack = new Stack<>();
    stack.push("hello");
    stack.push("world");
    stack.pop();
    stack.peek();
    stack.pop();
    try {
      stack.push(null);
    } catch(PreconditionViolationException e) {
      System.err.println(e);
    }
    try {
      stack.peek();
    } catch(PreconditionViolationException e) {
      System.out.println(e);
    }
    try {
      stack.pop();
    } catch(PreconditionViolationException e) {
      System.out.println(e);
    }
    try {
      new Stack<String>(null);
    } catch(PreconditionViolationException e) {
      System.out.println(e);
    }
  }
}
