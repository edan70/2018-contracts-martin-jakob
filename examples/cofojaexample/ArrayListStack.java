package examples.cofojaexample;

import conditions.pre.*;
import java.util.ArrayList;

public class ArrayListStack<T> implements Stack<T> {
  protected ArrayList<T> elements;

  public ArrayListStack() {
    elements = new ArrayList<T>();
  }

  @Override
  public int size() {
    return elements.size();
  }

  @Override
  public T peek() {
    return elements.get(elements.size() - 1);
  }

  @Override
  public T pop() {
    return elements.remove(elements.size() - 1);
  }

  @Override
  public void push(T obj) {
    elements.add(obj);
  }

  @Override
  public String toString() {
    return elements.toString();
  }
}
