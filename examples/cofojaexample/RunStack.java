package examples.cofojaexample;

import conditions.pre.*;
import java.util.Scanner;

public class RunStack {
  
  private static Scanner scan = new Scanner(System.in);
  private static Stack<String> stack = new ArrayListStack<String>();

  public static void main(String[] args) {
    while (true) {
      try {
        performOperation();
        System.out.print("Current stack: ");
        System.out.println(stack);
      } catch (PreconditionViolationException e) {
        System.out.println("Precondition violated: " + e.getMessage());
      }
    }
  }

  public static void performOperation() {
    System.out.println("\nChoose operation (push, pop, peek, size, quit)");
    String input = scan.nextLine();
    switch (input) {
      case "push":
        pushElem();
        break;    
      case "pop":
        popElem();
        break;
      case "peek":
        peekElem();
        break;
      case "size":
        printSize();
        break;
      case "quit":
        System.exit(0);
    }
  }

  public static void pushElem() {
    System.out.println("Type an element to push");
    String elem = scan.nextLine();
    stack.push(elem);
  }

  public static void popElem() {
    System.out.println("Popping element: " + stack.pop());
  }

  public static void peekElem() {
    System.out.println("Element on top of stack: " + stack.peek());
  }

  public static void printSize() {
    System.out.println("Current size is: " + stack.size());
  }
}
