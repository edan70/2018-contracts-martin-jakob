package examples.cofojaexample;

import conditions.pre.*;

public interface Stack<T> {

  public int size();

  @Requires({"notEmpty"})
  public T peek();

  @Requires({"notEmpty"})
  public T pop();

  public void push(@NotNull T obj);

  default public boolean notEmpty() {
    return size() >= 1;
  }
}
