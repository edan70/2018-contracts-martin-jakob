package org.extendj;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.List;

import beaver.Parser;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.fail;
import static com.google.common.truth.Truth.assertThat;

import org.extendj.JavaCompiler;

@RunWith(Parameterized.class)
public class CompileFailTest {
  private final File file;
  private final String filename;

  public CompileFailTest(File file, String filename) {
    this.file = file;
    this.filename = filename;
  }

  /** Build the list of test parameters (test input files). */
  @Parameterized.Parameters(name="{1}")
  public static Iterable<Object[]> getTests() {
    Collection<Object[]> tests = new LinkedList<>();
    addTests(tests, "testfiles/compile-errors");
    return tests;
  }

  private static void addTests(Collection<Object[]> tests, String dirPath) {
    File testDir = new File(dirPath);
    if (!testDir.isDirectory()) {
      throw new Error("Could not find the test directory '" + testDir + "'");
    }
    File[] files = testDir.listFiles();
    if (files != null) {
      for (File file : files) {
        if (file.getName().endsWith(".java")) {
          tests.add(new Object[] {file, file.getName()});
        }
      }
    }
  }

  @Test
  public void runTest() throws IOException, Parser.Exception, InterruptedException {
    File directory = file.getParentFile();
    String classname = file.getName();
    classname = classname.substring(0, classname.length() - 5);
    String conditions = "conditions/conditions.jar";

    try (FileReader reader = new FileReader(file)) {
      String[] args = {
        "-d", directory.getPath(),
        "-cp", conditions,
        file.getPath(),
      };
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      System.setErr(new PrintStream(baos));
			JavaCompiler.compile(args);
      String[] errorMessages = parseErrorMessage(baos.toString());
      if (errorMessages.length != 0) {
				assertThat(Arrays.asList(errorMessages))
          .containsExactlyElementsIn(getExpectedLines())
          .inOrder();
      } else {
        fail("Testcase should not compile.");
      }
    }
  }

  private static String[] parseErrorMessage(String message) {
    String[] messages = message.split("\n");
    for(int i = 0; i < messages.length; i++) {
      messages[i] = messages[i].replaceAll("(.*)java:[0-9]+: error: ", "");
    }
    return messages;
  }

  public List<String> getExpectedLines() throws IOException {
    List<String> expected = new LinkedList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      boolean addLines = false;
      while (true) {
        String line = reader.readLine();
        if (line == null) {
          break;
        } else if (addLines) {
          if (line.startsWith("*/")) {
            break;
          }
          expected.add(line);
        } else if (line.startsWith("/*EXPECT")) {
          addLines = true;
        }
      }
    }
    return expected;
  }
}
