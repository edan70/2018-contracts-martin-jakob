import conditions.pre.*;

public class BooleanMethodInAnotherClass {
  public static void main(String[] args) {
    try {
      new A().m("");
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    new A().m("a");
  }
}

class A {
  @Requires({"precond"})
  public void m(String s) {
    System.out.println("passed");
  }
}

class Util {
  public static boolean precond(String s) {
    return s.length() > 0;
  }
}

/*EXPECT
The precondition method 'precond' can not be found
*/
