import conditions.pre.MinValue;
import conditions.pre.MaxValue;

class ConcreteClass1 extends AbstractClass implements I2 {
  public int f(int a) {
    return a;
  }
}

class ConcreteClass2 implements I2 {
  public int f(int a) {
    return a;
  }
}

abstract class AbstractClass {
  public abstract int f(@MinValue(0) int a);
}

interface I1 {
  int f(@MaxValue(100) int a);
}

interface I2 extends I1 {
  int f(int a);
}

interface I3 {
  int f(int a);
}

/*EXPECTED
Multiple ancestors (AbstractClass, I1) must not contain any preconditions for method 'f'
*/
