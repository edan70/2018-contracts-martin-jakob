import conditions.pre.NotNull;

interface A {
  void m(Object o);
}

interface B {
  void m(@NotNull Object o);
}

class C implements A, B {
  public void m(Object o) {

  }
}

/*EXPECT
Multiple ancestors (A, B) must not contain any preconditions for method 'm'
*/
