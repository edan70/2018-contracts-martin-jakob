import conditions.pre.MaxValue;

public class MaxValueOnNonNumeral {
  public int func(@MaxValue(0) boolean b) {
    return 0;
  }

  public void func2(@MaxValue(-10) char c) { }

  public boolean func3(@MaxValue(100) String str) {
    return false;
  }

  public void func4(@MaxValue(2) Object o) { }

  public void func5(@MaxValue(0) int i) { }

  public void func6(@MaxValue(0) Integer i) { }
}

/*EXPECT
MaxValue can not target argument of type boolean
MaxValue can not target argument of type String
MaxValue can not target argument of type Object
*/
