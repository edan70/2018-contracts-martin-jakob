import conditions.pre.MinValue;

public class MinValueOnNonNumeral {
  public int func(@MinValue(0) boolean b) {
    return 0;
  }

  public void func2(@MinValue(-10) char c) { }

  public boolean func3(@MinValue(100) String str) {
    return false;
  }

  public void func4(@MinValue(2) Object o) { }

  public void func5(@MinValue(0) int i) { }

  public void func6(@MinValue(0) Integer i) { }
}

/*EXPECT
MinValue can not target argument of type boolean
MinValue can not target argument of type String
MinValue can not target argument of type Object
*/
