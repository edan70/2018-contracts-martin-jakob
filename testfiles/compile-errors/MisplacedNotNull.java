import conditions.pre.NotNull;

public class Test {
    
    public static void func2(@NotNull Object o) {

    }

    @NotNull
    public static void func(Object obj) {
        func2(obj);
    }

		public static void main(String[] args) {
		
		}
}

/*EXPECT
annotation type conditions.pre.NotNull is not applicable to this kind of declaration
*/
