import conditions.pre.Requires;

public class NonExistentBoolMethod {
  @Requires({"nonExistent"})
  public void m() {

  }
}

/*EXPECT
The precondition method 'nonExistent' can not be found
*/
