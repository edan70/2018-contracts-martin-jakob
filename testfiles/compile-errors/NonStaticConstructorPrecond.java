import conditions.pre.*;

public class NonStaticConstructorPrecond {
  @Requires({"precond"})
  public NonStaticConstructorPrecond() {

  }
  private boolean precond() {
    return true;
  }
}

/*EXPECT
The precondition method 'precond' must be static since it is called from a constructor
*/
