import conditions.pre.NotNull;

public class Test {
	public static void main(String[] args) { }

	public static void func(@NotNull int i) { }
	public static void func2(@NotNull boolean b) { }
}

/*EXPECT
NotNull can not target primitive argument 'i'
NotNull can not target primitive argument 'b'
*/
