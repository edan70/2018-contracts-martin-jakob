import conditions.pre.Not;

public class NotOnNonNumeral {
  public int func(@Not(0) boolean b) {
    return 0;
  }

  public void func2(@Not(-10) char c) { }

  public boolean func3(@Not(100) String str) {
    return false;
  }

  public void func4(@Not(2) Object o) { }

  public void func5(@Not(0) int i) { }

  public void func6(@Not(0) Integer i) { }
}

/*EXPECT
Not can not target argument of type boolean
Not can not target argument of type String
Not can not target argument of type Object
*/
