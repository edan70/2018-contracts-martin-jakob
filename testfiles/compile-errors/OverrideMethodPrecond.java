import conditions.pre.Requires;

public class OverrideMethodPrecond implements T {
  @Requires({"positiveNums"})
  public int add(int x, int y) {
    return x + y;
  }
}

interface T {
  public default boolean positiveNums(int x, int y) {
    return x > 0 && y > 0;
  }

  int add(int x, int y);
}

/*EXPECT
Subclass 'OverrideMethodPrecond' can not introduce new precondition to overrided function 'add(int, int)'
*/
