import conditions.pre.Requires;
import conditions.pre.Not;
import conditions.pre.PreconditionViolationException;

public class RequiresNonStaticFromStatic {

  public static void main(String[] args) {

    assert(DivMath.divide(10, 5) == 2);

    try {
      DivMath.divide(10, 0); // Violating Not(0)
    } catch(PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }

    try {
      DivMath.divide(10, 3); // Violating 'divisible'
    } catch(PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }

}

public class DivMath {
  @Requires({"divisible"})
  static int divide(int num, @Not(0) int denom) {
    return num / denom;
  }

  boolean divisible(int num, int denom) {
    return num % denom == 0;
  }
}

/*EXPECT
The non-static precondition method 'divisible' is called from a static context
*/
