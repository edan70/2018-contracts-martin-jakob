import conditions.pre.Not;

public class C implements A, B {
  public double div(double a, double b) {
    return a / b; 
  }
}

interface A {
  double div(double a, @Not(0) double b);
}

interface B {
  double div(double a, @Not(0) double b);
}

/*EXPECT
Multiple ancestors (A, B) must not contain any preconditions for method 'div'
*/
