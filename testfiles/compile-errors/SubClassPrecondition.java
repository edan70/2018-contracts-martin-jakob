import conditions.pre.NotNull;

class TestSubClassPrecondition {
  public class A {
    public void a(Object o) {
      
    }
  }

  public class B extends A {
    public void a(@NotNull Object o) {
      
    }
  }
}

/*EXPECT
Subclass 'B' can not introduce new precondition to overrided function 'a(java.lang.Object)'
*/
