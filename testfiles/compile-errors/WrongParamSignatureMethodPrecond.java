import conditions.pre.Requires;

public class WrongParamSignatureMethodPrecond {
  private boolean wrongParamSignature() {
    return true;
  }
  @Requires({"wrongParamSignature"})
  public void m(double d) {

  }
}

/*EXPECT
The precondition method 'wrongParamSignature' has wrong parameter signature
*/
