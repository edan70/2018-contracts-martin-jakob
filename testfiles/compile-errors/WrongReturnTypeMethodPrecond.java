import conditions.pre.Requires;

public class WrongReturnTypeMethodPrecond {
  private int wrongReturnType(double d) {
    return (int) d;
  }
  @Requires({"wrongReturnType"})
  public void m(double d) {

  }
}

/*EXPECT
The precondition method 'wrongReturnType' does not return a boolean type
*/
