import conditions.pre.MaxValue;
import conditions.pre.PreconditionViolationException;

public class BreakMaxValue {
  private int n = 0;

  public void incr(@MaxValue(value=1) int x) {
    n += x;
  }

  public static void floatMinVal(@MaxValue(1234.5678) float x) {

  }

  public static void main(String[] args) {
    BreakMaxValue obj = new BreakMaxValue();
    try {
      obj.incr(0);
      obj.incr(2);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      floatMinVal(1234);
      floatMinVal(1235);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

/*EXPECT
Argument 'x' must be less than or equal to 1
Argument 'x' must be less than or equal to 1234.5678
*/
