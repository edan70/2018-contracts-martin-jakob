import conditions.pre.MinValue;
import conditions.pre.PreconditionViolationException;

public class BreakMinValue {
  private int n = 0;

  public void incr(@MinValue(value=1) int x) {
    n += x;
  }

  public static void floatMinVal(@MinValue(1234.5678) float x) {

  }

  public static void main(String[] args) {
    BreakMinValue obj = new BreakMinValue();
    try {
      obj.incr(2);
      obj.incr(0);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      floatMinVal(1235);
      floatMinVal(1234);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

/*EXPECT
Argument 'x' must be greater than or equal to 1
Argument 'x' must be greater than or equal to 1234.5678
*/
