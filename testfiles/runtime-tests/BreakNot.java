import conditions.pre.Not;
import conditions.pre.PreconditionViolationException;

public class BreakNot {
  private int n = 0;

  public double divide(double num, @Not(value=0) double denom) {
    return num / denom;
  }

  public static void main(String[] args) {
    BreakNot obj = new BreakNot();
    obj.divide(3.41, -12.433);
    obj.divide(0, 1);
    obj.divide(12.3, 0.1);
    try {
      obj.divide(3, 0.0f);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      obj.divide(3, 0.0);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      obj.divide(3, 0);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

/*EXPECT
Argument 'denom' must not be equal to 0
Argument 'denom' must not be equal to 0
Argument 'denom' must not be equal to 0
*/
