import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

public class BreakingNotNull {

  public void func(@NotNull Object o) {
    System.out.println("This should not be printed");
  }

  public static void main(String[] args) {
    BreakingNotNull a = new BreakingNotNull();
    Object o1 = null;
    try {
      a.func(o1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

/*EXPECT
Argument 'o' must not be null
*/
