import conditions.pre.*;

public class ConstructorPrecond {

  public static void main(String[] args) {
    new ConstructorPrecond("a", 1);
    try {
      new ConstructorPrecond(null, 1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      new ConstructorPrecond("", 1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      new ConstructorPrecond("a", -1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Requires({"precond"})
  public ConstructorPrecond(@NotNull String s, int x) {
    System.out.println("passed");
  }

  private static boolean precond(String s, int x) {
    return s.length() > 0 && x > 0;
  }
}

/*EXPECT
passed
Argument 's' must not be null
Method precondition 'precond' not satisfied
Method precondition 'precond' not satisfied
*/