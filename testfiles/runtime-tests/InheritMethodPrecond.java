import conditions.pre.Requires;
import conditions.pre.PreconditionViolationException;

public class InheritMethodPrecond extends I {
  public static void main(String[] args) {
    try {
      new InheritMethodPrecond().func(0.001);
      new InheritMethodPrecond().func(-1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
  public int func(double d) {
    System.out.println("func passed");
    return (int) d;
  }
}
  
abstract class I {
  public boolean largerThanZero(double d) {
    return d > 0;
  }

  @Requires({"largerThanZero"})
  int func(double d) {
    return 0;
  }
}

/*EXPECT
func passed
Method precondition 'largerThanZero' not satisfied
*/
