import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

public class InheritanceStatic {
  public static void main(String[] args) {
    try {
      B.a(false);
      B.a(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

class A {
  public static void a(Boolean b) {

  }
}

class B extends A {
  public static void a(@NotNull Boolean b) {
    // Precondition in subclass is okay since the method is static.
    System.out.println("hello world");
  }
}

/*EXPECT
hello world
Argument 'b' must not be null
*/
