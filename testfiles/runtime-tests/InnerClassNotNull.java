import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

interface AbstractClass {
  void callInnerFunc(Object o);
}


public class InnerClassNotNull {
  public static void main(String[] args) {
    AbstractClass ac = new AbstractClass() {
      public void callInnerFunc(Object o) {
        func(o);
      }

      public void func(@NotNull Object o) {
        System.out.println("this should not be printed.");
      }
    };
    try {
      ac.callInnerFunc(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

/*EXPECT
Argument 'o' must not be null
*/
