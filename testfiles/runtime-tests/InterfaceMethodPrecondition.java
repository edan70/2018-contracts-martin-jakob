import conditions.pre.PreconditionViolationException;
import conditions.pre.Requires;

public class InterfaceMethodPrecondition implements Y {
  public static void main(String[] args) {
    try {
      new InterfaceMethodPrecondition().func(1);
      new InterfaceMethodPrecondition().func(3);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
  public int func(int x) {
    System.out.println(x);
    return 0;
  }
}

interface Y {
  default boolean notThree(int x) {
    return x != 3;
  }

  @Requires({"notThree"})
  int func(int x);
}

/*EXPECT
1
Method precondition 'notThree' not satisfied
*/
