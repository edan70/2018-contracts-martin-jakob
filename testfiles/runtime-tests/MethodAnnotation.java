import conditions.pre.Requires;
import conditions.pre.PreconditionViolationException;

public class MethodAnnotation {

  public static void main(String[] args) {
    try {
      new MethodAnnotation().m(0);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }

  @Requires({"checkState1", "checkState2"})
  public void m(int i) {

  }

  public boolean checkState1(int i) {
    System.out.println("Check precond 1");
    return true;
  }

  public boolean checkState2(int i) {
    System.out.println("Check precond 2");
    return false;
  }
}

/*EXPECT
Check precond 1
Check precond 2
Method precondition 'checkState2' not satisfied
*/
