import conditions.pre.*;

public class MethodAnnotation2 {
  
  private String someAttribute = "attribute";
  private int x;

  public static void main(String[] args) {
    doCase(1, "a");
    doCase(0, "a");
    doCase(-1, "");
    doCase(-1, "attribute");
    doCase(1, "");
    doCase(1, "attribute");
  }

  private static void doCase(int constructorParam, String mParam) {
    try {
      new MethodAnnotation2(constructorParam).m(mParam);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }

  public MethodAnnotation2(int x) {
    this.x = x;
  }

  @Requires({"xGreaterThanZero", "notEmptyString", "notEqualToAttribute"})
  public void m(String s) {
    System.out.println("m passed");
  }

  private boolean xGreaterThanZero(String s) {
    return x > 0;
  }

  private boolean notEmptyString(String s) {
    return s.length() > 0;
  }

  private boolean notEqualToAttribute(String s) {
    return !s.equals(someAttribute);
  }
}

/*EXPECT
m passed
Method precondition 'xGreaterThanZero' not satisfied
Method precondition 'xGreaterThanZero' not satisfied
Method precondition 'xGreaterThanZero' not satisfied
Method precondition 'notEmptyString' not satisfied
Method precondition 'notEqualToAttribute' not satisfied
*/
