import conditions.pre.NotNull;

public class MissingAnnotationInSubClass {
  public static void main(String[] args) {

  }
}

class S {
  public void func(@NotNull Object o) {
    
  }
}

class T extends S {
  public void func(Object o) {
      
  }
}
