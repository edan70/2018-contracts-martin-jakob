public class NotAPrecondition {

    public @interface Random { }

    public static void func(@Random Object o) {
        System.out.println("foo");
    }
    
    public static void main(String[] args) {
         func(null);
         System.out.println("bar");
    }
}

/*EXPECT
foo
bar
*/
