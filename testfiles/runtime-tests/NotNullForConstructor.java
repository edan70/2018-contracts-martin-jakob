public class NotNullForConstructor {

  public static void main(String[] args) {
    try {
      B b = new B(null);
    } catch (conditions.pre.PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

class B {
  public B(@conditions.pre.NotNull Object foo) {
    System.out.println("Constructor: this should not be printed");
  }
}

/*EXPECT
Argument 'foo' must not be null
*/
