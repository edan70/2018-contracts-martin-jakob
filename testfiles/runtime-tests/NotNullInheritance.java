import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

public class NotNullInheritance {
  public static void main(String[] args) {
    SuperClass sc = new SubClass();
    try {
      sc.func(new Object());
      sc.func(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    
    sc = new SubClassOverride();
    try {
      sc.func(new Object());
      sc.func(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

class SuperClass {
  public void func(@NotNull Object param) {
    System.out.println("called func");
  }
}

class SubClass extends SuperClass {

}

class SubClassOverride extends SuperClass {

  @Override
  public void func(Object newName) {
    System.out.println("called overrided func");
  }
}

/*EXPECT
called func
Argument 'param' must not be null
called overrided func
Argument 'newName' must not be null
*/
