import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

public class NotNullNonPrimitives {
    public static void a(@NotNull Integer a) {

    }

    public static void b(@NotNull Boolean b) {

    }

    public static void c(@NotNull Float c) {

    }

    public static void d(@NotNull Character d) {

    }

    public static void e(@NotNull Byte e) {

    }

    public static void f(@NotNull Short f) {

    }

    public static void main(String[] args) {
        try {
            a(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
        try {
            b(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
        try {
            c(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
        try {
            d(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
        try {
            e(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
        try {
            f(null);
        } catch (PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
    }
}

/*EXPECT
Argument 'a' must not be null
Argument 'b' must not be null
Argument 'c' must not be null
Argument 'd' must not be null
Argument 'e' must not be null
Argument 'f' must not be null
*/
