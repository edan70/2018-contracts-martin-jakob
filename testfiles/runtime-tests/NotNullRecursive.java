import conditions.pre.NotNull;

public class NotNullRecursive {

    public static void main(String[] args) {
        A a = new A(10);
        try {
            a.func();
            System.out.println("This should not be printed.");
        } catch (conditions.pre.PreconditionViolationException e) {
            System.out.println(e.getMessage());
        }
    }
}

class A {
    private int n;

    public A(@NotNull Integer n) {
        this.n = n;
    }

    public A func() {
        if (n > 0) {
            System.out.println(n);
            return new A(n - 1).func();
        } else {
            return new A(null);
        }
    }
}

/*EXPECT
10
9
8
7
6
5
4
3
2
1
Argument 'n' must not be null
*/
