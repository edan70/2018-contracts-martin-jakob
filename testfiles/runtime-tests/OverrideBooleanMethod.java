import conditions.pre.*;

public class OverrideBooleanMethod {
  public static void main(String[] args) {
    new B().m("d");
    try {
      new B().m(" ");
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    try {
      new B().m("");
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    } 
    new A().m(" ");
    try {
      new A().m("");
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    } 
  }
}

class B extends A {
  protected boolean notEmpty(String s) {
    return !s.matches("\\s*");
  }
  public void m(String s) {
    System.out.println("B:" + s);
  }
}

class A {
  protected boolean notEmpty(String s) {
    return s.length() > 0;
  }
  @Requires({"notEmpty"})
  public void m(String s) {
    System.out.println("A:" + s);
  }
}

/*EXPECT
B:d
Method precondition 'notEmpty' not satisfied
Method precondition 'notEmpty' not satisfied
A: 
Method precondition 'notEmpty' not satisfied
*/ 