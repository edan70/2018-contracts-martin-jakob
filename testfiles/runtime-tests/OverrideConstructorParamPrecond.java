import conditions.pre.NotNull;
import conditions.pre.MinValue;
import conditions.pre.PreconditionViolationException;

public class OverrideConstructorParamPrecond {
  public static void main(String[] args) {
    new Subtype(0);
    try {
      new Subtype(null);
    } catch (NullPointerException e) {
      System.out.println("null pointer");
    }
    try {
      new Subtype(-1);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
    new Ancestor(-1);
    try {
      new Ancestor(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

class Ancestor {
  public Ancestor(@NotNull Integer k) {
    System.out.println("ancestor");
  }
}

class Subtype extends Ancestor {
  public Subtype(@MinValue(0) Integer i) {
    super(i);
    System.out.println("subtype");
  }
}

/*EXPECT
ancestor
subtype
null pointer
Argument 'i' must be greater than or equal to 0
ancestor
Argument 'k' must not be null
*/
