import conditions.pre.NotNull;
import conditions.pre.PreconditionViolationException;

public class OverrideConstructorPreCond {
  public static void main(String[] args) {
    try {
      new C(0);
      new C(null);
      new B(0);
      new B(null);
    } catch (PreconditionViolationException e) {
      System.out.println(e.getMessage());
    }
  }
}

class A {
  public A(@NotNull Integer i) {
    System.out.println("Calling A with argument " + i);
  }
}

class B extends A {
  public B(Integer k) {
    super(k);
    System.out.println("Calling B with argument " + k);
  }
}

class C extends B {
  public C(Integer n) {
    super(1);
    System.out.println("Calling C with argument " + n);
  }
}

/*EXPECT
Calling A with argument 1
Calling B with argument 1
Calling C with argument 0
Calling A with argument 1
Calling B with argument 1
Calling C with argument null
Calling A with argument 0
Calling B with argument 0
Argument 'i' must not be null
*/
