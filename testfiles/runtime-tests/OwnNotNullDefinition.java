public class OwnNotNullDefinition {

    public @interface NotNull { }

    public static void func(@NotNull Object o) {
        System.out.println("foo");
    }
    
    public static void main(String[] args) {
         func(null);
         System.out.println("bar");
    }
}

/*EXPECT
foo
bar
*/
